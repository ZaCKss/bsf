#include <pco.h>

void PCO_basic::set_pin(uint8_t input_pin, uint8_t shutdown_pin, uint8_t buzzer_pin) {
  _inputPin = input_pin;
  _shutdownPin = _shutdownPin;
  _buzzerPin = buzzer_pin;
}

uint8_t PCO_basic::read_input(void) {
  return digitalRead(_inputPin);
}

void PCO_basic::shutdown(void) {
  digitalWrite(_shutdownPin, HIGH);
}

void PCO_basic::buzzer_alarm(uint8_t rep, uint8_t duration){
  for(uint8_t i = 0; i < rep; i++){
    digitalWrite(_buzzerPin, HIGH);
    delay(duration);
    digitalWrite(_buzzerPin, LOW);
    if(i < rep - 1){
      delay(duration);
    }
  }
}

bool PCO_advance::set_hold_time(uint32_t arr[MAX_OUTPUT]) {
  for (uint8_t i = 0; i < MAX_OUTPUT - 1; i++) {
    if (arr[i] >= arr[i] + 1) {
      return false;
    }
  }
  return true;
}

void PCO_advance::loop(void) {
  static uint8_t btn_command = 0;
  bool btn_flag;
  uint32_t button_tick;

  /* button handler */
  if (read_input() == LOW) {
    button_press_handle(true, &btn_flag, &button_tick);
    button_timer_handle(button_tick, &btn_command);
  } else {
    button_press_handle(false, &btn_flag, &button_tick);
    button_timer_handle(button_tick, &btn_command, false);
  }

  _btnCommand = btn_command;
  _outputFlag = btn_flag;

  // if (!button_flag && btn_command == 3) {
  //   shutdown();
  // }
}

void PCO_advance::reset_command(void) {
  _btnCommand = 0;
}

uint8_t PCO_advance::get_command(void) {
  return _btnCommand;
}

bool PCO_advance::get_flag(void) {
  return _outputFlag;
}

void PCO_advance::button_press_handle(bool state, bool *output_flag, uint32_t *tick) {
  static bool prev_state = false;
  if (state != prev_state) {
    prev_state = state;
    if (state) { /* if button pressed */
      *output_flag = true;
      *tick = millis();
    } else {
      *output_flag = false;
    }
  }
}

void PCO_advance::button_timer_handle(uint32_t tick, uint8_t *btn_cmd, bool is_button_pressed) {
  for (uint8_t i = 0; i < MAX_OUTPUT; i++) {
    if (millis() - tick > _btnHoldTime[i]) {
      static bool beep_done = false;
      if (!is_button_pressed) {
        /* reset state after button not pressed*/
        beep_done = false;
      }
      if (!beep_done && is_button_pressed) {
        beep_done = true;
        *btn_cmd = i + 1;
      }
    }
  }
}