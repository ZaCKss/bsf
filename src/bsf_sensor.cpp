#include <bsf_lib.h>

Adafruit_AHTX0 aht;
RTC_DS3231 rtc;
sensors_event_t event_temp, event_humidity;
OneWire onewire(PIN_DS18B20);
DallasTemperature ds18b20(&onewire);

/**
 * @brief sensor initialization
 * @param none
 * @return return error code. 0x00 if success, else fail
*/
uint8_t sensor_begin(void) {
  uint8_t initNum = 0;
  uint8_t ret = 0;
  /* Wire.begin return true if success, so invert it */
  if (!return_status_decode(!Wire.begin(I2C_SDA, I2C_SCL), false, false)) {
    ret |= (0x01 << initNum);
  }
  initNum++;

  /* RTC init */
  if (!return_status_decode(!rtc.begin(&Wire), false, false)) {
    ret |= (0x01 << initNum);
  }
  initNum++;

  if (rtc.lostPower()) {
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  
  /* AHT init */
  if (!return_status_decode(!aht.begin(&Wire), false, false)) {
    ret |= (0x01 << initNum);
  }
  initNum++;

  /* DS18B20 init */
  ds18b20.begin();
  if (!return_status_decode(!ds18b20.getDeviceCount(), false, false)) {
    ret |= (0x01 << initNum);
  }
  initNum++;

  return ret;
}

/**
 * @brief read AHT10 temperature
 * @param none
 * @return ambient temperature (in celcius)
*/
float read_ambient_temp(void) {
  aht.getEvent(&event_humidity, &event_temp);
  return event_temp.temperature;
}

/**
 * @brief read AHT10 humidity
 * @param none
 * @return ambient humidity, range 0~100%
*/
float read_ambient_humi(void) {
  aht.getEvent(&event_humidity, &event_temp);
  return event_humidity.relative_humidity;
}

/**
 * @brief read LDR lux value
 * @param pin LDR pin ID
 * @return lux value
*/
float read_ldr_lux(uint8_t pin) {
  /* read resistor pulldown voltage */
  float r_voltage = read_analog_voltage(pin) / 1000;
  /* read LDR voltage */
  float voltage = 3.3 - r_voltage;
  /* read LDR resistance */
  float resistance = voltage / r_voltage * LDR_PULLDOWN_RESISTOR;

  return LUX_SCALAR * pow(resistance, LUX_EXPONENT);
}

/**
 * @brief read soil humidity
 * @param pin soil moisture sensor pin ID
 * @return soil humidity, range 0~100%
*/
float read_soil_humi(uint8_t pin) {
  return map(analogRead(pin), SOIL_DRY_VALUE, SOIL_WET_VALUE, 0, 100);
}

/**
 * @brief read soil temperature
 * @param none
 * @return soil temperature in celcius
*/
float read_soil_temp(void) {
  return ds18b20.getTempCByIndex(0);
}