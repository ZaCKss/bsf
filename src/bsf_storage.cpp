#include <bsf_lib.h>

/**
 * @brief file system initialization
 * @param none
 * @return 0x00 success, else: fail
*/
uint8_t fs_init(){
  uint8_t ret = 0;
  uint8_t initNum = 0;

  if(!SPIFFS.begin(true)){
    ret |= (0x01 << initNum);
  }
  initNum++;

  return ret;
}

/**
 * @brief read file system data
 * @param path file path
 * @return file data in string
*/
String fs_read(const char* path){
  String data = "null";
  File myFile = SPIFFS.open(path, FILE_READ);
  if(myFile){
    data = myFile.readString();
  }
  myFile.close();
  return data;
}

/**
 * @brief write data to file system
 * @param data data to write
 * @param path file path
 * @param mode FILE_APPEND for append, FILE_WRITE to overwrite
 * @return 0x00 if success, else: fail
*/
uint8_t fs_write(const char* data, const char* path, const char* mode){
  uint8_t ret = 0;
  uint8_t initNum = 0;
  File myFile = SPIFFS.open(path, mode, true);
  if(myFile){
    myFile.print(data);
    myFile.close();
  }
  else{
    ret |= (0x01 << initNum);
  }
  initNum++;

  return ret;
}