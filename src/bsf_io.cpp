#include <bsf_lib.h>

/**
 * @brief IO initialization
 * @param none
 * @return none
*/
uint8_t io_init(){
  pinMode(PIN_LED_R, OUTPUT);
  pinMode(PIN_LED_B, OUTPUT);
  pinMode(PIN_LED_B, OUTPUT);
  pinMode(PIN_LDR, INPUT);
  pinMode(PIN_SOIL, INPUT);
  pinMode(PIN_BUZZER, OUTPUT);
  pinMode(PIN_BUTTON, INPUT);
  pinMode(PIN_SHUTDOWN, OUTPUT);
  ledcSetup(PWM_CHANNEL_0, PWM_FREQ, PWM_RESOLUTION);
  ledcAttachPin(PIN_FAN, PWM_CHANNEL_0);

  set_pwm(PWM_CHANNEL_0, 0);

  return 0x00;
}

/**
 * @brief set pwm 8-bit value
 * @param channel pwm channel
 * @param pwm 8-bit pwm value
*/
void set_pwm(uint8_t channel, uint8_t pwm){
  ledcWrite(channel, pwm);
}

/**
 * @brief set output pin to HIGH or LOW
 * @param pin IO pin number
 * @param logic output logic
 * @return none
*/
void set_io_state(uint8_t pin, bool logic){
  digitalWrite(pin, logic);
}

/**
 * @brief beep buzzer at decided cycle and duration
 * @param rep beep cycle(s)
 * @param duration ON/OFF duration in ms
 * @return none
*/
void buzzer_alarm(uint8_t rep, uint8_t duration){
  for(uint8_t i = 0; i < rep; i++){
    set_io_state(PIN_BUZZER, HIGH);
    delay(duration);
    set_io_state(PIN_BUZZER, LOW);
    if(i < rep - 1){
      delay(duration);
    }
  }
}

/**
 * @brief read input status
 * @param pin IO pin number
 * @return true: pin status HIGH, false: pin status LOW
*/
bool read_io_state(uint8_t pin){
  return digitalRead(pin);
}

/**
 * @brief turn off all IO
 * @param none
 * @return none
*/
void io_cleanup(){
  set_io_state(PIN_LED_R, LOW);
  set_io_state(PIN_LED_G, LOW);
  set_io_state(PIN_LED_B, LOW);
  set_pwm(PWM_CHANNEL_0, 0);
}

/**
 * @brief get pin voltage in milivolts
 * @param none
 * @return none
*/
uint32_t read_analog_voltage(uint8_t pin) {
  return analogReadMilliVolts(pin);
}