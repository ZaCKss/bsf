#include <bsf_lib.h>

TaskHandle_t handler_task_main;
TaskHandle_t handler_task_sensor;

machine_state_t machine_state;
sensor_data_t sensor_data;

extern RTC_DS3231 rtc;

/**
 * @brief RTOS task initialization
 * @param none
 * @return none
*/
void task_init(void) {
  xTaskCreatePinnedToCore(task_main, "task_main", TASK_DEPTH, NULL, PRIORITY_TASK_MAIN, &handler_task_main, CORE_TASK_MAIN);
  xTaskCreatePinnedToCore(task_sensor, "task_sensor", TASK_DEPTH, NULL, PRIORITY_TASK_SENSOR, &handler_task_sensor, CORE_TASK_SENSOR);
}

/**
 * @brief main task function
 * @param none
 * @return none
*/
void task_main(void* param) {
  while(true) {
    /* 3 mode fan speed: 0 fan off. 1 half speed. 2 full speed */
    static uint8_t fan_speed = 0;
    static uint8_t btn_command = 0;
    bool btn_flag;
    uint32_t button_tick;

    /* button handle */
    if (read_io_state(PIN_BUTTON) == LOW) {
      button_press_handle(true, &btn_flag, &button_tick);
      button_timer_handle(button_tick, &btn_command, machine_state);
    } else {
      button_press_handle(false, &btn_flag, &button_tick);
      button_timer_handle(button_tick, &btn_command, machine_state, false);
    }

    /* process handle */
    if (!btn_flag && btn_command == 1) {
      fan_speed++;
      if (fan_speed > 2) {
        fan_speed = 0;
      }
      sensor_data.fan_speed = FAN_MAX_SPEED * fan_speed / 2;
      btn_command = 0;
      debug_log(LOG_LEVEL_INFO, LBL_INFO "Fan Speed: %s%u%s\r\n", DEBUG_COLOR_SET_CYAN, fan_speed, DEBUG_COLOR_RESET);
    } else if (!btn_flag && btn_command == 2) {
      if (machine_state == MACHINE_IDDLE && fan_speed != 0) {
        /* things to do when machine running, execute only once*/
        machine_state = MACHINE_RUNNING;
        sensor_data.start_time = sensor_data.epoch_time;
        debug_log(LOG_LEVEL_WARN, LBL_INFO "Deleting log file...");
        return_status_decode(!SPIFFS.remove(LOG_DEFAULT_PATH), true, true);
        fs_write(String("Fan Speed,Media Humidiy,Media Temperature,Ambient Humidity,Ambient Temperature,Light Intensity,Time\n").c_str(), LOG_DEFAULT_PATH, FILE_APPEND);
        set_pwm(PWM_CHANNEL_0, 255 * fan_speed / 2);
        
        debug_log(LOG_LEVEL_INFO, LBL_INFO DEBUG_COLOR_GREEN_BOLD("Machine Running") "\r\n");
      } else if (machine_state == MACHINE_RUNNING && fan_speed != 0) {
        machine_state = MACHINE_IDDLE;
        debug_log(LOG_LEVEL_INFO, LBL_INFO DEBUG_COLOR_RED_BOLD("Machine Stop") "\r\n");
      } else {
        debug_log(LOG_LEVEL_INFO, LBL_INFO DEBUG_COLOR_YELLOW("Fan speed can't be 0 !") "\r\n");
      }
      btn_command = 0;
    } else if (!btn_flag && btn_command == 3) { /* shutdown */
      debug_log(LOG_LEVEL_INFO, LBL_INFO DEBUG_COLOR_RED_BOLD("Shutting Down") "\r\n");
      vTaskDelay(500);
      set_io_state(PIN_SHUTDOWN, HIGH);
      btn_command = 0;
    }

    /* debug print handler */
    if (machine_state == MACHINE_RUNNING) {
      static uint32_t tick;

      String temp = String(sensor_data.fan_speed) + ",";
      temp += String(sensor_data.object_humidity) + ",";
      temp += String(sensor_data.object_temperature) + ",";
      temp += String(sensor_data.ambient_humidity) + ",";
      temp += String(sensor_data.ambient_temperature) + ",";
      temp += String(sensor_data.lux) + ",";
      temp += String(sensor_data.epoch_time - sensor_data.start_time) + "\n";

      if (millis() - tick > DEBUG_OUTPUT_TIME) {
        debug_log(LOG_LEVEL_DEBUG, LBL_DEBUG "Data: ");
        DEBUG.print(temp);
        tick = millis();
      }
    }
    
    /* handle serial input */
    com_debug_handler();
    server_loop();
  }
}

/**
 * @brief sensor task function
 * @param none
 * @return none
*/
void task_sensor(void* param) {
  while(true) {
    DateTime now = rtc.now();
    sensor_data.ambient_humidity = read_ambient_humi();
    sensor_data.ambient_temperature = read_ambient_temp();
    // sensor_data.object_temperature = read_soil_temp();
    // sensor_data.object_humidity = read_soil_humi(PIN_SOIL);
    // sensor_data.lux = read_ldr_lux(PIN_LDR);
    sensor_data.epoch_time = now.unixtime();

    /* logging handler */
    if (machine_state == MACHINE_RUNNING) {
      static uint32_t tick;
      if (millis() - tick > LOGGING_TIME) {
        String temp = String(sensor_data.fan_speed) + ",";
        temp += String(sensor_data.object_humidity) + ",";
        temp += String(sensor_data.object_temperature) + ",";
        temp += String(sensor_data.ambient_humidity) + ",";
        temp += String(sensor_data.ambient_temperature) + ",";
        temp += String(sensor_data.lux) + ",";
        temp += String(sensor_data.epoch_time - sensor_data.start_time) + "\n";

        debug_log(LOG_LEVEL_INFO, LBL_INFO "Saving log...");
        return_status_decode(fs_write(temp.c_str(), LOG_DEFAULT_PATH, FILE_APPEND), true, true);

        tick = millis();
      }
    }
  }
}

/**
 * @brief handle button press duration
 * @param tick time when button is pressed
 * @param btn_cmd pointer to store command after button released
 * @param is_button_pressed pretty self explanatory
 * @return none
*/
void button_timer_handle(uint32_t tick, uint8_t *btn_cmd, machine_state_t state, bool is_button_pressed) {
  if (millis() - tick > SELECT_TIME_THRESHOLD && state != MACHINE_RUNNING) {
    static bool beep_done = false;
    if (!is_button_pressed) {
      /* reset state after button not pressed*/
      beep_done = false;
    }
    if (!beep_done && is_button_pressed) {
      buzzer_alarm(1);
      beep_done = true;
      *btn_cmd = 1;
    }
  }

  if (millis() - tick > START_TIME_THRESHOLD) {
    static bool beep_done = false;
    if (!is_button_pressed) {
      /* reset state after button not pressed*/
      beep_done = false;
    }
    if (!beep_done && is_button_pressed) {
      buzzer_alarm(2);
      beep_done = true;
      *btn_cmd = 2;
    }
  }

  if (millis() - tick > SHUTDOWN_TIME_THRESHOLD) {
    static bool beep_done = false;
    if (!is_button_pressed) {
      beep_done = false;
    }
    if (!beep_done && is_button_pressed) {
      buzzer_alarm(3);
      beep_done = true;
      *btn_cmd = 3;
    }
  }
}

/**
 * @brief handle button input for HIGH LOW change
 * @param button_state time when button is pressed
 * @param output_flag pointer filtered output
 * @param tick store time when button is pressed
 * @return none
*/
void button_press_handle(bool state, bool *output_flag, uint32_t *tick) {
  static bool prev_state = false;
  if (state != prev_state) {
    prev_state = state;
    if (state) { /* if button pressed */
      *output_flag = true;
      *tick = millis();
    } else {
      *output_flag = false;
    }
  }
}

int check_status(void) {
  StaticJsonDocument<1024> doc;
  DeserializationError error = deserializeJson(doc, fs_read(CONFIG_PATH));
  if (!error) {
    return doc["status"].as<int>();
  } else {
    return -1;
  }
}

/**
 * @brief watchdog feeder
 * @param none
 * @return none
*/
void watchdog_feed(void) {
  TIMERG0.wdt_wprotect = TIMG_WDT_WKEY_VALUE;
  TIMERG0.wdt_feed = 1;
  TIMERG0.wdt_wprotect = 0;
  TIMERG1.wdt_wprotect = TIMG_WDT_WKEY_VALUE;
  TIMERG1.wdt_feed = 1;
  TIMERG1.wdt_wprotect = 0;
}