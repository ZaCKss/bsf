#include <bsf_lib.h>

ESP32WebServer server(80);

/**
 * @brief Communication function initialization
 * @param none
 * @return 0x00: success, else: error code
 */
uint8_t com_init(void) {
  uint8_t ret;
  uint8_t initNum;

  if(!WiFi.softAP(AP_SSID, AP_PASS)){
    ret |= (0x01 << initNum);
  }
  initNum++;
  delay(500);

  WiFi.setTxPower(WIFI_POWER_5dBm);

  if(!MDNS.begin(SERVER_NAME)){
    ret |= (0x01 << initNum);
  }
  initNum++;

  server.on("/", server_handler);
  server.begin();

  return ret;
}

/**
 * @brief Console debug log
 * @param level intended debug level
 * @param message debug message
 * @return none
 */
void debug_log(const uint8_t level, const __FlashStringHelper *message) {
  debug_log(level, "%s", (const char *)message);
}

/**
 * @brief Console debug log
 * @param level intended debug level
 * @param message debug message
 * @param args message argument
 * @return none
 */
void debug_log(const uint8_t level, const char *message, ...) {
  if (level > DEBUG_LOG_LEVEL) {
    return;
  }

  char buffer[256];
  va_list args;
  va_start(args, message);

  vsnprintf(buffer, sizeof(buffer), message, args);
  DEBUG.print(buffer);
}

/**
 * @brief web server handler for file download
 * @param none
 * @return none
*/
void server_handler(void) {
  debug_log(LOG_LEVEL_INFO, LBL_INFO "Sending file...");
  File myFile = SPIFFS.open(LOG_DEFAULT_PATH, FILE_READ);
  String file_name = LOG_DEFAULT_PATH;
  file_name.replace("/", "");

  if (myFile) {
    server.sendHeader("Content-Type", "text/text");
    server.sendHeader("Content-Disposition", "attachment; filename=" + file_name);
    server.sendHeader("Connection", "close");
    server.streamFile(myFile, "application/octet-stream");
    myFile.close();
    debug_log(LOG_LEVEL_INFO, LBL_OK "\r\n");
  } else {
    debug_log(LOG_LEVEL_INFO, LBL_FAIL "\r\n");
  }
}

/**
 * @brief webserver client handle
 * @param none
 * @return none
*/
void server_loop(void) {
  server.handleClient();
}

void com_debug_handler(void) {
  if (DEBUG.available()) {
    String cmd = DEBUG.readStringUntil('\n');
    cmd = string_cleanup(cmd);

    if (cmd.equals("read_log")) {
      String coba = fs_read(LOG_DEFAULT_PATH);
      debug_log(LOG_LEVEL_DEBUG, LBL_DEBUG "FS Data: ");
      DEBUG.println(coba);
    } else if (cmd.equals("delete_log")) {
      if(SPIFFS.remove(LOG_DEFAULT_PATH)) {
        debug_log(LOG_LEVEL_WARN, LBL_INFO DEBUG_COLOR_YELLOW("Log File Deleted") "\r\n");
      }
    } else if (cmd.equals("test")) {
      debug_log(LOG_LEVEL_WARN, LBL_OK "\r\n");
    }
  }
}

/**
 * @brief remove CR and LF from given string
 * @param str input string
 * @return removed CR and LF string
*/
String string_cleanup(String str) {
  str.replace("\r", "\0");
  str.replace("\n", "\0");
  str.trim();

  return str;
}