#include <bsf_lib.h>
/*test*/
/**
 * @brief device initialization
 * @param none
 * @return none
*/
uint8_t device_init(void) {
  uint8_t ret = 0;
  uint8_t initNum = 0;

  DEBUG.begin(115200);
  debug_log(LOG_LEVEL_INFO, DEBUG_COLOR_MAGENTA("BSF Controller v1.0") "\r\n");
  debug_log(LOG_LEVEL_INFO, LBL_INFO "UID: %s", DEBUG_COLOR_SET_MAGENTA);
  DEBUG.print(get_uid());
  DEBUG.println(DEBUG_COLOR_RESET);
  debug_log(LOG_LEVEL_INFO, LBL_INFO "GPIO Init...");
  if(!return_status_decode(io_init(), true, true)){
    ret |= (0x01 << initNum);
  }
  initNum++;

  debug_log(LOG_LEVEL_INFO, LBL_INFO "FS Init...");
  if(!return_status_decode(fs_init(), true, true)){
    ret |= (0x01 << initNum);
  }
  initNum++;

  debug_log(LOG_LEVEL_INFO, LBL_INFO "Sensor Init...");
  if (!return_status_decode(sensor_begin(), true, true)) {
    ret |= (0x01 << initNum);
  }
  initNum++;

  debug_log(LOG_LEVEL_INFO, LBL_INFO "Server Init...");
  if (!return_status_decode(com_init(), true, true)) {
    ret |= (0x01 << initNum);
  }
  initNum++;

  debug_log(LOG_LEVEL_INFO, LBL_INFO "WiFi Power: %u\r\n", WiFi.getTxPower());

  return ret;
}

/**
 * @brief Get device UID
 * @param printConsole print to debug console
 * @return Device UID
 */
String get_uid(const bool printConsole) {
  char UID[30];
  uint64_t chipID = ESP.getEfuseMac();
  uint16_t mfrID = (uint16_t)(chipID >> 32);
  sprintf(UID, "%s_%04X%08X", ID, mfrID, (uint32_t)chipID);

  if (printConsole) debug_log(LOG_LEVEL_DEBUG, "Device UID = %s\r\n", UID);

  return String(UID);
}

/**
 * @brief Decode return code status
 * @param returnCode code value
 * @param printConsole print to debug console
 * @param newline print newline after code value
 * @return true: success, false: fail
 */
bool return_status_decode(const uint8_t returnCode, const bool printConsole,
                          const bool newline) {
  bool ret = false;

  if (returnCode == 0x00) {
    if (printConsole) debug_log(LOG_LEVEL_INFO, F(LBL_OK));
    ret = true;
  } else {
    if (printConsole) debug_log(LOG_LEVEL_INFO, F(LBL_FAIL));
    ret = false;
  }

  if (printConsole)
    debug_log(LOG_LEVEL_INFO, " [0x%02X]%s", returnCode,
              (newline ? "\r\n" : ""));

  return ret;
}