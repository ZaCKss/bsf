#ifndef PCO_H
#define PCO_H

#define MAX_OUTPUT 0x03

#include <Arduino.h>

class PCO_basic{
public:
  void set_pin(uint8_t input_pin, uint8_t shutdown_pin = 0, uint8_t buzzer_pin = 0);
  uint8_t read_input(void);
  void shutdown(void);
  void buzzer_alarm(uint8_t rep, uint8_t duration);
private:
  uint8_t _inputPin;
  uint8_t _shutdownPin;
  uint8_t _buzzerPin;
};

class PCO_advance: public PCO_basic{
public:
  bool set_hold_time(uint32_t arr[MAX_OUTPUT]);
  void loop(void);
  void reset_command(void);
  uint8_t get_command(void);
  bool get_flag(void);
private:
  void button_press_handle(bool state, bool *output_flag, uint32_t *tick);
  void button_timer_handle(uint32_t tick, uint8_t *btn_cmd, bool is_button_pressed = true);

  uint32_t _btnHoldTime[MAX_OUTPUT];
  uint8_t _btnCommand = 0;
  bool _outputFlag;
};

#endif