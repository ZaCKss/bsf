#ifndef BSF_STORAGE_H
#define BSF_STORAGE_H

#define LOG_DEFAULT_PATH "/BSF_log.csv"
#define CONFIG_PATH "/BSF_config.json"
#define LOG_TIME_STAMP 60

#include <Arduino.h>
#include <SPIFFS.h>

uint8_t fs_init(void);
String fs_read(const char* path);
uint8_t fs_write(const char* data, const char* path, const char* mode);

#endif