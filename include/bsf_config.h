#ifndef BSF_CONFIG_H
#define BSF_CONFIG_H

#define ID "BSF"

#include <Arduino.h>

uint8_t device_init(void);
String get_uid(const bool printConsole);
bool return_status_decode(const uint8_t returnCode, const bool printConsole, const bool newline);

#endif