#ifndef BSF_IO_H
#define BSF_IO_H

#define PIN_LED_R 14
#define PIN_LED_G 27
#define PIN_LED_B 26

#define PIN_LDR 36
#define PIN_SOIL 34

#define PIN_FAN 19
// #define PIN_BUZZER 18
#define PIN_BUZZER 32

// #define PIN_BUTTON 23
#define PIN_BUTTON 33
#define PIN_SHUTDOWN 22

#define PWM_CHANNEL_0 0 /* Channel Fan */
// #define PWM_FREQ  980
#define PWM_FREQ  5000
#define PWM_RESOLUTION  8

#define FAN_MAX_SPEED 43.56

#include <Arduino.h>

uint8_t io_init(void);
void set_io_state(uint8_t pin, bool logic);
void set_pwm(uint8_t channel, uint8_t pwm);
void buzzer_alarm(uint8_t rep, uint8_t duration = 100);
bool read_io_state(uint8_t pin);
void io_cleanup(void);

uint32_t read_analog_voltage(uint8_t pin);

#endif