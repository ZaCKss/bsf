#ifndef BSF_COM_H
#define BSF_COM_H

enum LOG_LEVEL {
  LOG_LEVEL_NONE,
  LOG_LEVEL_ERROR,
  LOG_LEVEL_WARN,
  LOG_LEVEL_INFO,
  LOG_LEVEL_DEBUG,
  LOG_LEVEL_VERBOSE
};

#define DEBUG_LOG_LEVEL LOG_LEVEL_DEBUG

#define DEBUG_ANSI_OUTPUT true

#define DEBUG Serial
#define DEBUG_BAUD 115200

#define AP_SSID "BSF WiFi"
#define AP_PASS "12345678"
#define SERVER_NAME "BSF_WEB"

#ifdef DEBUG_ANSI_OUTPUT
#define DEBUG_COLOR_RED_BOLD(letter) "\033[1m\033[31m" letter "\033[0m"
#define DEBUG_COLOR_GREEN_BOLD(letter) "\033[1m\033[32m" letter "\033[0m"
#define DEBUG_COLOR_YELLOW(letter) "\033[33m" letter "\033[0m"
#define DEBUG_COLOR_BLUE(letter) "\033[34m" letter "\033[0m"
#define DEBUG_COLOR_MAGENTA(letter) "\033[35m" letter "\033[0m"
#define DEBUG_COLOR_CYAN(letter) "\033[36m" letter "\033[0m"
#define DEBUG_COLOR_WHITE_BOLD(letter) "\033[1m\033[37m" letter "\033[0m"

#define DEBUG_COLOR_SET_RED_BOLD "\033[1m\033[31m"
#define DEBUG_COLOR_SET_GREEN_BOLD "\033[1m\033[32m"
#define DEBUG_COLOR_SET_YELLOW "\033[33m"
#define DEBUG_COLOR_SET_BLUE "\033[34m"
#define DEBUG_COLOR_SET_MAGENTA "\033[35m"
#define DEBUG_COLOR_SET_CYAN "\033[36m"
#define DEBUG_COLOR_SET_WHITE_BOLD "\033[1m\033[37m"
#define DEBUG_COLOR_RESET "\033[0m"

#define LBL_OK DEBUG_COLOR_GREEN_BOLD("OK")
#define LBL_FAIL DEBUG_COLOR_RED_BOLD("FAIL")
#define LBL_DEBUG DEBUG_COLOR_WHITE_BOLD("DEBUG >> ")
#define LBL_INFO DEBUG_COLOR_CYAN("[INFO] ")
#define LBL_WARN DEBUG_COLOR_YELLOW("[WARNING] ")
#define LBL_VERBOSE DEBUG_COLOR_MAGENTA("[VERBOSE] ")
#else
#define LBL_OK "OK"
#define LBL_FAIL "FAILED"
#define LBL_DEBUG "DEBUG >> "
#define LBL_INFO "[info] "
#define LBL_WARN "[warn] "
#define LBL_VERBOSE "[verbose] "
#endif

#include <Arduino.h>
#include <WiFi.h>
#include <ESP32WebServer.h> // https://github.com/Pedroalbuquerque/ESP32WebServer
#include <ESPmDNS.h>

uint8_t com_init(void);
void debug_log(const uint8_t level, const char *message, ...);
void debug_log(const uint8_t level, const __FlashStringHelper *message);
void listen_command(void);
void server_handler(void);
void server_loop(void);
void com_debug_handler(void);
String get_uid(const bool printConsole = false);
String string_cleanup(String str);

#endif