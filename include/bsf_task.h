#ifndef BSF_TASK_H
#define BSF_TASK_H

enum MACHINE_STATE{
  MACHINE_IDDLE,
  MACHINE_RUNNING
};

typedef uint8_t machine_state_t;

typedef struct {
  float fan_speed;
  float ambient_temperature;
  float ambient_humidity;
  float lux;
  float object_temperature;
  float object_humidity;
  uint32_t epoch_time;
  uint32_t start_time;
}sensor_data_t;

#define CORE_TASK_MAIN 0x00
#define CORE_TASK_SENSOR 0x01
#define PRIORITY_TASK_MAIN 0x02
#define PRIORITY_TASK_SENSOR 0x01
#define TASK_DEPTH 16384

#define SHUTDOWN_TIME_THRESHOLD 5000 /* 5 seconds */
#define START_TIME_THRESHOLD 2000 /* 2 seconds */
#define SELECT_TIME_THRESHOLD 200 /* 0.2 second */
#define LOGGING_TIME 1 * 60 * 1000 /* 1 minute */
#define DEBUG_OUTPUT_TIME 1000 /* 1 seconnd */

#include <Arduino.h>
#include <ArduinoJson.h>
#include <soc/timer_group_reg.h>
#include <soc/timer_group_struct.h>

void task_init(void);
void task_main(void* param);
void task_sensor(void* param);

void button_timer_handle(uint32_t tick, uint8_t *btn_cmd, machine_state_t state, bool is_button_pressed = true);
void button_press_handle(bool state, bool *output_flag, uint32_t *tick);

int check_status(void);

void watchdog_feed(void);

#endif