#ifndef BSF_SENSOR_H
#define BSF_SENSOR_H

// #define I2C_SCL 33
// #define I2C_SDA 25
#define I2C_SCL 22
#define I2C_SDA 21

#define LDR_PULLDOWN_RESISTOR 1000
#define LUX_SCALAR 12518931
#define LUX_EXPONENT -1.405

#define SOIL_DRY_VALUE 400
#define SOIL_WET_VALUE 500

#define PIN_DS18B20 29

#include <Arduino.h>
#include <RTClib.h>
#include <Adafruit_AHTX0.h>
#include <OneWire.h>
#include <DallasTemperature.h>

uint8_t sensor_begin(void);
float read_ambient_temp(void);
float read_ambient_humi(void);

float read_ldr_lux(uint8_t pin);
float read_soil_humi(uint8_t pin);
float read_soil_temp(void);

#endif